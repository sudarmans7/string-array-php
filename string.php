<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php
        echo "<h3>Soal No 1</h3>";
        $kalimat1 = "PHP is never old";
        echo "Kalimat 1 : " . $kalimat1 . "<br>";
        echo "Panjang String : " . strlen($kalimat1) . "<br>";
        echo "Jumlah Kata : " . str_word_count($kalimat1) . "<br>";

        echo "<h3>Soal No 2</h3>";
        $kalimat2 = "I Love PHP";
        echo "kalimat : " . $kalimat2 . "<br>";
        echo "kata 1 : " . substr($kalimat2,0,1). "<br>";
        echo "kata 2 : " . substr($kalimat2,1,5). "<br>";
        echo "kata 3 : " . substr($kalimat2,6,5). "<br>";

        echo "<h3>Soal No 3</h3>";
        $kalimat3 = "PHP is old but Good!";
        echo "Kalimat 3 : " .$kalimat3 . "<br>";
        echo "kalimat 3 di ubah : " .str_replace("PHP is old but Good!","PHP is old but awesome", $kalimat3);
    
    ?>

</body>
</html>